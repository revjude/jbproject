-- jbproject create script
--		USE jbproject
--		GO

--create utility schema
		IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = 'utility')
		BEGIN
			DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA utility'
			EXEC (@cSQL)
		END
		GO



--utility.DropObject - utility prod for dropping an object before creating it.  useful for rerunable sql because it runs whether an object already exists or not
		/****** Object:  StoredProcedure [utility].[DropObject]    Script Date: 2/10/2018 7:37:27 PM ******/
		IF EXISTS (SELECT object_id FROM sys.objects O WHERE name = 'dropobject')
			DROP PROCEDURE [utility].[DropObject]
		GO
		/****** Object:  StoredProcedure [utility].[DropObject]    Script Date: 2/10/2018 7:37:27 PM ******/
		SET ANSI_NULLS ON
		GO

		SET QUOTED_IDENTIFIER ON
		GO

		CREATE PROCEDURE [utility].[DropObject]
		@ObjectName VARCHAR(MAX)

		AS
		BEGIN

		DECLARE @cSQL VARCHAR(MAX)
		DECLARE @cType VARCHAR(10)

		IF CHARINDEX('.', @ObjectName) = 0 AND NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = @ObjectName)
			SET @ObjectName = 'dbo.' + @ObjectName
		--ENDIF

		SELECT @cType = O.Type
		FROM sys.objects O 
		WHERE O.Object_ID = OBJECT_ID(@ObjectName)

		IF @cType IS NOT NULL
			BEGIN
			
			IF @cType IN ('D', 'PK')
				BEGIN
				
				SELECT
					@cSQL = 'ALTER TABLE ' + S2.Name + '.' + O2.Name + ' DROP CONSTRAINT ' + O1.Name
				FROM sys.objects O1
					JOIN sys.Schemas S1 ON S1.Schema_ID = O1.Schema_ID
					JOIN sys.objects O2 ON O2.Object_ID = O1.Parent_Object_ID
					JOIN sys.Schemas S2 ON S2.Schema_ID = O2.Schema_ID
						AND S1.Name + '.' + O1.Name = @ObjectName
					
				EXEC (@cSQL)
				
				END
			ELSE IF @cType IN ('FN','IF','TF','FS','FT')
				BEGIN
				
				SET @cSQL = 'DROP FUNCTION ' + @ObjectName
				EXEC (@cSQL)
				
				END
			ELSE IF @cType IN ('P','PC')
				BEGIN
				
				SET @cSQL = 'DROP PROCEDURE ' + @ObjectName
				EXEC (@cSQL)
				
				END
			ELSE IF @cType = 'SN'
				BEGIN
				
				SET @cSQL = 'DROP SYNONYM ' + @ObjectName
				EXEC (@cSQL)
				
				END
			ELSE IF @cType = 'TR'
				BEGIN
				
				SET @cSQL = 'DROP TRIGGER ' + @ObjectName
				EXEC (@cSQL)
				
				END
			ELSE IF @cType = 'U'
				BEGIN
				
				SET @cSQL = 'DROP TABLE ' + @ObjectName
				EXEC (@cSQL)
				
				END
			ELSE IF @cType = 'V'
				BEGIN
				
				SET @cSQL = 'DROP VIEW ' + @ObjectName
				EXEC (@cSQL)
				
				END
			--ENDIF

			END
		ELSE IF EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = @ObjectName)
			BEGIN

			SET @cSQL = 'DROP SCHEMA ' + @ObjectName
			EXEC (@cSQL)

			END
		ELSE IF EXISTS (SELECT 1 FROM sys.types T JOIN sys.schemas S ON S.Schema_ID = T.Schema_ID AND S.Name + '.' + T.Name = @ObjectName)
			BEGIN

			SET @cSQL = 'DROP TYPE ' + @ObjectName
			EXEC (@cSQL)

			END
		--ENDIF
				
		END	
		GO

--utility.SetPrimaryKeyNonClustered - utility procedure for setting clustered primary key on a table
		EXEC utility.DropObject 'utility.SetPrimaryKeyNonClustered'
		GO
		/****** Object:  StoredProcedure [utility].[SetPrimaryKeyNonClustered]    Script Date: 2/10/2018 8:48:18 PM ******/
		SET ANSI_NULLS ON
		GO

		SET QUOTED_IDENTIFIER ON
		GO

		CREATE PROCEDURE [utility].[SetPrimaryKeyNonClustered]

		@TableName VARCHAR(250),
		@ColumnName VARCHAR(250)

		AS
		BEGIN
			SET NOCOUNT ON;

			DECLARE @cSQL VARCHAR(MAX)
			DECLARE @nLength INT
			
			IF CHARINDEX('.', @TableName) = 0
				SET @TableName = 'dbo.' + @TableName
			--ENDIF

			SET @nLength = LEN(@TableName) - CHARINDEX('.', @TableName)
			SET @cSQL = 'ALTER TABLE ' + @TableName + ' ADD CONSTRAINT PK_' + RIGHT(@TableName, @nLength) + ' PRIMARY KEY NONCLUSTERED (' + @ColumnName + ')'

			EXECUTE (@cSQL)

		END
		GO


--utility.SetIndexClustered - utility procedure for setting clustered index
		EXEC utility.DropObject 'utility.SetIndexClustered'
		GO
		/****** Object:  StoredProcedure [utility].[SetIndexClustered]    Script Date: 2/10/2018 8:25:43 PM ******/
		SET ANSI_NULLS ON
		GO
		SET QUOTED_IDENTIFIER ON
		GO

		CREATE PROCEDURE [utility].[SetIndexClustered]

		@TableName VARCHAR(250),
		@IndexName VARCHAR(250),
		@Columns VARCHAR(MAX)

		AS
		BEGIN
			SET NOCOUNT ON;

			DECLARE @cSQL VARCHAR(MAX)
			
			IF CHARINDEX('.', @TableName) = 0
				SET @TableName = 'dbo.' + @TableName
			--ENDIF

			SET @cSQL = 'CREATE CLUSTERED INDEX ' + @IndexName + ' ON ' + @TableName + ' (' + @Columns + ') WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON)'

			EXECUTE (@cSQL)

		END
		GO


-- utility.AddSchema - utility procedure for adding a new schema
		EXEC utility.DropObject 'utility.AddSchema'
		GO

		/****** Object:  StoredProcedure [utility].[AddSchema]    Script Date: 2/10/2018 7:50:18 PM ******/
		SET ANSI_NULLS ON
		GO

		SET QUOTED_IDENTIFIER ON
		GO

		CREATE PROCEDURE [utility].[AddSchema]

		@SchemaName VARCHAR(250)

		AS
		BEGIN
			SET NOCOUNT ON;

			IF NOT EXISTS (SELECT 1 FROM sys.schemas S WHERE S.Name = @SchemaName)
				BEGIN
			
				DECLARE @cSQL VARCHAR(MAX) = 'CREATE SCHEMA ' + LOWER(@SchemaName)
			
				EXEC (@cSQL)
			
				END
			--ENDIF

		END
		GO




--add the surevey schema for the new table
		EXEC utility.AddSchema 'survey'
		GO


--Begin table survey.SurveySubmission
		DECLARE @TableName VARCHAR(250) = 'survey.SurveySubmission'

		EXEC utility.DropObject @TableName

		CREATE TABLE survey.SurveySubmission
			(
			SurveySubmissionID INT IDENTITY(1,1) NOT NULL,
			Make VARCHAR(50),
			Color VARCHAR(50),
			Transmission VARCHAR(50),
			IPAddress VARCHAR(50)
			)

		EXEC utility.SetPrimaryKeyNonClustered @TableName, 'SurveySubmissionID'
		EXEC utility.SetIndexClustered @TableName, 'IX_IPAddress', 'IPAddress'
		GO
		--End table survey.SurveySubmission




--survey.GetSurveySubmissionByIPAddress - procedure that returns submission data by IP address
		EXEC Utility.DropObject 'survey.GetSurveySubmissionByIPAddress'
		GO

		SET ANSI_NULLS ON
		GO
		SET QUOTED_IDENTIFIER ON
		GO

		CREATE PROCEDURE survey.GetSurveySubmissionByIPAddress

		@IPAddress VARCHAR(50)

		AS
		BEGIN
			SET NOCOUNT ON;

			SELECT 
				SS.SurveySubmissionID,
				SS.Make,
				SS.Color,
				SS.Transmission
			FROM survey.SurveySubmission SS
			WHERE SS.IPAddress = @IPAddress
			
		END
		GO
		--End procedure survey.GetSurveySubmissionByIPAddress


--survey.DeleteSurveySubmissionByIPAddress - procedure that returns submission data by IP address
		EXEC Utility.DropObject 'survey.DeleteSurveySubmissionByIPAddress'
		GO

		SET ANSI_NULLS ON
		GO
		SET QUOTED_IDENTIFIER ON
		GO

		CREATE PROCEDURE survey.DeleteSurveySubmissionByIPAddress

		@IPAddress VARCHAR(50)

		AS
		BEGIN
			SET NOCOUNT ON;

			DELETE FROM survey.SurveySubmission
			WHERE IPAddress = @IPAddress
			
		END
		GO
		--End procedure survey.DeleteSurveySubmissionByIPAddress


--survey.SubmitSurvey - procedure that returns submission data by IP address
		EXEC Utility.DropObject 'survey.SubmitSurvey'
		GO

		SET ANSI_NULLS ON
		GO
		SET QUOTED_IDENTIFIER ON
		GO

		CREATE PROCEDURE survey.SubmitSurvey

		@Make VARCHAR(50),
		@Color VARCHAR(50),
		@Transmission VARCHAR(50),
		@IPAddress VARCHAR(50)

		AS
		BEGIN
			SET NOCOUNT ON;

			INSERT INTO survey.SurveySubmission(Make,Color,Transmission,IPAddress)
			VALUES(@Make,@Color,@Transmission,@IPAddress)
			
		END
		GO
		--End procedure survey.SubmitSurvey