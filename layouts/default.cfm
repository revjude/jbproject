<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<title>SignUpGenius Interview Project - <cfoutput>#rc.title#</cfoutput></title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />

	<!-- Includes Styles -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
	<link href="/assets/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css" rel="stylesheet" />
	<link href="/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
	<link href="/assets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" />
	<link href="/assets/css/animate.css" rel="stylesheet" />
	<link href="/assets/css/style.css" rel="stylesheet" />
	<link href="/assets/css/style-responsive.css" rel="stylesheet" />
	<link href="/assets/css/theme/default.css" rel="stylesheet" id="theme" />


	<!-- Include page level styles -->
	<cfset variables.includedCSS = {}>
	<cfloop index="variables.i" to="#arrayLen(rc.cssIncludes)#" from="1">
		<cfset variables.cssInclude =  rc.cssIncludes[variables.i]>
		<cfif !StructKeyExists(variables.includedCSS,variables.cssInclude)>
			<cfset variables.cachebuster = find('?',variables.cssInclude) ? '&#application.cachebuster#' : '?#application.cachebuster#'>
			<link rel="stylesheet" href="<cfoutput>#variables.cssInclude##variables.cachebuster#</cfoutput>" type="text/css">
			<cfset variables.includedCSS[variables.cssInclude] = "">
		</cfif>
	</cfloop>

	<!-- JS that controlls page loader progress bar -->
	<script src="/assets/plugins/pace/pace.min.js"></script>
</head>

<body class="pace-top">

	<!-- page loader progress bar -->
	<div id="page-loader" class="fade in"><span class="spinner"></span></div>

	<a href="/"><img src="/assets/img/logo.png" class="m-l-20 m-t-20" width="250"></a>
	<div id="page-container" class="fade">

		<!-- include request level view -->
		<cfoutput>#body#</cfoutput>

	    <!-- include project side-bar on every view -->
	    <div class="theme-panel">
	        <a href="javascript:;" data-click="theme-panel-expand" class="theme-collapse-btn"><i class="fa fa-cog"></i></a>
	        <div class="theme-panel-content">
	            <h5 class="m-t-0">Author:<br>Jonathan Burnham</h5>
	            <div class="divider"></div>
	            <div class="row m-t-10">
	                <div class="col-md-12 control-label double-line">
	                	<a href="?action=main.servefile&file=JonathanBurnham.Resume.CFFullStack.pdf">Download My Resume</a>
	                </div>
	            </div>
	            <div class="row m-t-10">
	                <div class="col-md-12 control-label double-line">
	                	<a href="?action=main.servefile&file=JonathanBurnham.SignUpGenius.InterviewProject.zip">Download this Project</a>
	                </div>
	            </div>
	            <div class="row m-t-10">
	                <div class="col-md-12">
	                    <a href="?action=main.reset" class="btn btn-inverse btn-block btn-sm" ><i class="fa fa-refresh m-r-3"></i> Clear Survey</a>
	                </div>
	            </div>
	        </div>
	    </div>

	</div>

	<!-- include global JavaScripts -->
	<script src="/assets/plugins/jquery/jquery-1.9.1.min.js"></script>
	<script src="/assets/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
	<script src="/assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
	<script src="/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<!--[if lt IE 9]>
		<script src="/assets/crossbrowserjs/html5shiv.js"></script>
		<script src="/assets/crossbrowserjs/respond.min.js"></script>
		<script src="/assets/crossbrowserjs/excanvas.min.js"></script>
	<![endif]-->
	<script src="/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="/assets/plugins/jquery-cookie/jquery.cookie.js"></script>
	<script src="/assets/plugins/gritter/js/jquery.gritter.min.js" type="text/javascript"></script>

	<!-- include the main application js script that runs the template -->
	<script src="/assets/js/apps.js"></script>

	<script>
		$(document).ready(function() {
			//init the template
			App.init();

			//if there is a notificaiton to show, show it
			<cfif StructKeyExists(rc, "notification")>
				<cfset rc.notification.title = structKeyExists(rc.notification, "title") ? rc.notification.title : "">
				<cfset rc.notification.text = structKeyExists(rc.notification, "text") ? rc.notification.text : "">
				$.gritter.add({
					title: "<cfoutput>#jsStringFormat(rc.notification.title)#</cfoutput>",
					text: "<cfoutput>#jsStringFormat(rc.notification.text)#</cfoutput>"
				});
			</cfif>
		});

		//set all data coming from the cf request into a global var available to page level scripts
		<cfloop index="variables.nI" from="1" to="#arrayLen(rc.jsData)#">
			<cfoutput>#rc.jsData[variables.nI]#</cfoutput>
		</cfloop>
	</script>

	<!-- include page-level JS scripts -->
	<cfset variables.includedJS = {}>
	<cfloop index="variables.nI" from="1" to="#arrayLen(rc.jsIncludes)#">
		<cfset variables.jsInclude = rc.jsIncludes[variables.nI]>
		<cfif !StructKeyExists(variables.includedJS, variables.jsInclude)>
			<cfset variables.cachebuster = find('?',variables.jsInclude) ? '&#application.cachebuster#' : '?#application.cachebuster#'>
			<script src="<cfoutput>#variables.jsInclude##variables.cachebuster#</cfoutput>" type="text/javascript"></script>
			<cfset variables.includedJS[variables.jsInclude] = "">
		</cfif>
	</cfloop>

</body>
</html>