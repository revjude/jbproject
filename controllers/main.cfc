component accessors="true" {

	property surveyService;
	property fileService;

	public any function init( fw ) {
		variables.fw = fw;
		return this;
	}

	public void function default( rc ) {
		param name="rc.complete" default="false";
		param name="rc.reset" default="false";

		rc.title = "Car Survey";
		rc.qSurveySubmission = variables.surveyService.getSurveySubmissionByIPAddress(cgi.remote_addr).submission;

		if(rc.qSurveySubmission.recordcount GT 0){
			rc.complete = true;
			rc.title = "Thank you!";
		}

		if( rc.reset ){
			rc.notification = {
				title = "Your survey was removed!",
				text = "The survey associated with your IP address has been removed from the database.  You can now resubmit."
			};
		}
	}

	public void function submit( rc ) {
		variables.surveyService.submitSurvey( rc );
		variables.fw.redirect( action="main.default" );
	}

	public void function reset( rc ) {
		variables.surveyService.deleteSurveySubmissionByIPAddress(cgi.remote_addr);
		rc.reset = true;
		variables.fw.redirect( action="main.default", append="reset" );
	}

	public void function servefile( rc ) {
		variables.fileService.serveFile(rc.file);
	}

}