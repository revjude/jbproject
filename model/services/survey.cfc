component {

	public struct function getSurveySubmissionByIPAddress( required string cIPAddress ) {

		var spData = new StoredProc(
			procedure = "survey.GetSurveySubmissionByIPAddress",
			parameters = [
				{value=arguments.cIPAddress, cfsqltype="CF_SQL_VARCHAR"}
			],
			procResults = [
				{resultset=1, name="submission"}
			]
		).execute();

		return local.spData.getProcResultSets();
	}

	public void function deleteSurveySubmissionByIPAddress( required string cIPAddress ) {

		var spData = new StoredProc(
			procedure = "survey.DeleteSurveySubmissionByIPAddress",
			parameters = [
				{value=arguments.cIPAddress, cfsqltype="CF_SQL_VARCHAR"}
			]
		).execute();

	}

	public void function submitSurvey( required struct sData ) {

		var spData = new StoredProc(
			procedure = "survey.SubmitSurvey",
			parameters = [
				{value=arguments.sData.make, cfsqltype="CF_SQL_VARCHAR"},
				{value=arguments.sData.color, cfsqltype="CF_SQL_VARCHAR"},
				{value=arguments.sData.transmission, cfsqltype="CF_SQL_VARCHAR"},
				{value=arguments.sData.ipaddress, cfsqltype="CF_SQL_VARCHAR"}
			]
		).execute();
		
	}

}