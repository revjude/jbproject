component {

	public void function serveFile( required string file ) {

		var fileName = arguments.file;
		var filePath = expandPath("./assets/files/");
		var fileInfo = GetFileInfo(local.filePath&local.fileName);
		var fileType = "text/html";

		if(listLast(local.fileName, ".") == 'zip'){
			local.fileType = 'application/zip';
		} else if (listLast(local.fileName, ".") == 'pdf') {
			local.fileType = 'application/pdf';
		}

		cfheader(name="content-length", value="#local.fileInfo.size#");
		cfheader(name="Content-Disposition", charset="utf-8", value="attachment;filename=#chr(34)##arguments.file##chr(34)#");
		cfcontent(type="#local.fileType#", file="#local.filePath&local.fileName#");
	}

}