component extends="framework.one" {

	this.DataSource = "jbproject";
	this.Name = this.DataSource;
	this.SessionManagement = true;
	this.SessionTimeout = createTimeSpan(0,2,0,0);

	variables.framework = {
		reloadApplicationOnEveryRequest = true
	};

	public void function setupApplication() {
		application.cachebuster = ListLast(CreateUUID(),"-");
	}

	function before( struct rc ) {
		rc.title = "";
	}
	
	function setupRequest() {
		request.context.startTime = getTickCount();
		if (structKeyExists(url, "reinit")) {
			setupApplication();
		}
	}

	public void function setupView() {
		rc.cssIncludes = [];
		rc.jsData = [];
		rc.jsIncludes = [];
	}

}
