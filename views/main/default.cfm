<!--- add page-level css and js --->
<cfset arrayAppend(rc.jsIncludes, "/assets/js/main/default.js")>
<cfset arrayAppend(rc.cssIncludes, "/assets/css/main/default.css")>
<cfset arrayAppend(rc.jsIncludes, "/assets/plugins/bootstrap-multiselect/js/bootstrap-multiselect.js")>
<cfset arrayAppend(rc.cssIncludes, "/assets/plugins/bootstrap-multiselect/css/bootstrap-multiselect.css")>
<cfset arrayAppend(rc.jsData, "surveyComplete = #rc.complete#;")>

<div class="login bg-black animated fadeInDown">
	<div class="login-header">
		<div class="brand">
			<cfif rc.complete>
				Thank You!
				<small>Thank you for taking the time for our survery.  Your feedback helps us improve our service!</small>
			<cfelse>
				Take Our Survey...
				<small>Tell us about your car!</small>
			</cfif>
		</div>
	</div>
	<div class="center-stripe">
		<div class="login-content">
			<form action="?action=main.submit" method="POST" class="margin-bottom-0">
				<div class="row">
					<div class="col-lg-3">&nbsp;</div>
					<div class="col-lg-6 form-group m-b-20">
						<label>What brand of car do you drive?</label><br>
						<select multiple="multiple" name="make" id="make" class="form-control input-lg inverse-mode no-border">
							<option value="Chevy" <cfif rc.qSurveySubmission.Make IS "Chevy">selected="selected"</cfif>>Chevy</option>
							<option value="Ford" <cfif rc.qSurveySubmission.Make IS "Ford">selected="selected"</cfif>>Ford</option>
							<option value="Honda" <cfif rc.qSurveySubmission.Make IS "Honda">selected="selected"</cfif>>Honda</option>
							<option value="Buick" <cfif rc.qSurveySubmission.Make IS "Buick">selected="selected"</cfif>>Buick</option>
							<option value="Toyota" <cfif rc.qSurveySubmission.Make IS "Toyota">selected="selected"</cfif>>Toyota</option>
							<option value="Tesla" <cfif rc.qSurveySubmission.Make IS "Tesla">selected="selected"</cfif>>Tesla</option>
							<option value="Kia" <cfif rc.qSurveySubmission.Make IS "Kia">selected="selected"</cfif>>Kia</option>
						</select> 
					</div>
				</div>
				<div class="row hide" id="colorRow">
					<div class="col-lg-3">&nbsp;</div>
					<div class="col-lg-6 form-group m-b-20">
						<label>What color is your <span class="makeLabel">car</span>?</label><br>
						<select multiple="multiple" name="color" id="color" class="form-control input-lg inverse-mode no-border" required>
							<option value="blue" <cfif rc.qSurveySubmission.Color IS "blue">selected="selected"</cfif>>Blue</option>
							<option value="silver" <cfif rc.qSurveySubmission.Color IS "silver">selected="selected"</cfif>>Silver</option>
							<option value="black" <cfif rc.qSurveySubmission.Color IS "black">selected="selected"</cfif>>Black</option>
							<option value="white" <cfif rc.qSurveySubmission.Color IS "white">selected="selected"</cfif>>White</option>
							<option value="red" <cfif rc.qSurveySubmission.Color IS "red">selected="selected"</cfif>>Red</option>
							<option value="green" <cfif rc.qSurveySubmission.Color IS "green">selected="selected"</cfif>>Green</option>
							<option value="yellow" <cfif rc.qSurveySubmission.Color IS "yellow">selected="selected"</cfif>>Yellow</option>
							<option value="pink" <cfif rc.qSurveySubmission.Color IS "pink">selected="selected"</cfif>>Pink</option>
						</select> 
					</div>
				</div>
				<div class="row hide" id="transmissionDiv">
					<div class="col-lg-3">&nbsp;</div>
					<div class="col-lg-6 form-group m-b-20">
						<label>What kind of transmission does your <span class="makeLabel">car</span> have?</label>
						<div class="form-check">
							<input class="form-check-input" type="radio" name="transmission" id="manual" value="Manual" checked="<cfoutput>#rc.qSurveySubmission.Transmission IS "Manual" ? "true" : "false"#</cfoutput>">
							<label class="form-check-label" for="manual">
								Manual
							</label>
						</div>
						<div class="form-check">
							<input class="form-check-input" type="radio" name="transmission" id="automatic" value="Automatic" checked="<cfoutput>#rc.qSurveySubmission.Transmission IS "Automatic" ? "true" : "false"#</cfoutput>">
							<label class="form-check-label" for="automatic">
								Automatic
							</label>
						</div>
					</div>
				</div>
				<div class="row hide" id="submitRow">
					<div class="col-lg-3">&nbsp;</div>
					<div class="col-lg-6 login-buttons">
						<button type="submit" class="btn btn-success btn-block btn-lg <cfif rc.complete>disabled</cfif>" id="submitButton">
							<cfif rc.complete>
								Thank you!
							<cfelse>
								Submit!
							</cfif>
						</button>
					</div>
				</div>
				<input type="hidden" name="ipaddress" value="<cfoutput>#cgi.remote_addr#</cfoutput>">
			</form>
		</div>
	</div>
</div>