$(document).ready(function(){
	$('select#make').multiselect({
		buttonWidth: '100%',
		onChange: function(option, checked, select){
			$('input[name="transmission"]').prop('checked',false);
			$('select#color option:selected').each(function() {
				$(this).prop('selected', false);
			});
			$('select#color').multiselect('refresh');
			selectOne(option,$('select#make'));
		}
	});
	$('select#color').multiselect({
		buttonWidth: '100%',
		onChange: function(option, checked, select){
			$('input[name="transmission"]').prop('checked',false);
			selectOne(option,$('select#color'));
		}
	});
	$('input[name="transmission"]').change(function(){
		updateFormDisplay();
	});
	updateFormDisplay();
	if(surveyComplete){
		lockForm();
	}
});

function updateFormDisplay(){
	var $make = $('select#make');
	var $color = $('select#color');
	if($make.val()==null){
		$('div#colorRow').addClass('hide');
		$('select#color option:selected').each(function() {
			$(this).prop('selected', false);
		});
		$('select#color').multiselect('refresh');
		$('div#transmissionDiv').addClass('hide');
		$('input#manual').prop("checked",false);
		$('input#automatic').prop("checked",false);
		$('div#submitRow').addClass('hide');
		$('span.makeLabel').html('');
	} else {
		$('span.makeLabel').html($make.val());
		$('div#colorRow').removeClass('hide');
		if(($make.val()=='Toyota')||($make.val()=='Tesla')){
			$color.next().find('input[value="pink"], input[value="green"], input[value="yellow"]').each(function(){
				$(this).parent('label').parent('a').parent('li').addClass('hide');
			});
			if(($color.val()=='pink')||($color.val()=='yellow')||($color.val()=='green')){
				$('select#color option:selected').each(function() {
					$(this).prop('selected', false);
				});
				$('select#color').multiselect('refresh');
			}
		} else {
			$color.next().find('input[value="pink"], input[value="green"], input[value="yellow"]').each(function(){
				$(this).parent('label').parent('a').parent('li').removeClass('hide');
			});
		}
	}
	if(($make.val()=='Tesla')||($color.val()=='yellow')||($color.val()=='green')){
		$('div#transmissionDiv').addClass('hide');
		if($make.val()=='Tesla'){
			$('input#manual').prop("checked",true);
			$('input#automatic').prop("checked",false);
		} else {
			$('input#manual').prop("checked",false);
			$('input#automatic').prop("checked",true);
		}
	} else if($color.val()!=null){
		$('div#transmissionDiv').removeClass('hide');
	} else {
		$('div#transmissionDiv').addClass('hide');
	}
	if(($color.val()!=null)&&($('input#manual').prop("checked")||$('input#automatic').prop("checked"))){
		$('div#submitRow').removeClass('hide');
	} else {
		$('div#submitRow').addClass('hide');
	}
};

function selectOne(option, $select){
    var values = [];
    $select.find('option').each(function() {
        if ($(this).val() !== option.val()) {
            values.push($(this).val());
        }
    });
    $select.multiselect('deselect', values);
    $select.next().removeClass('open'); 
	updateFormDisplay();
}

function lockForm(){
	$('#submitButton').attr('type','button');
	$('input#manual').prop("checked",false);
	$('input[name="transmission"]').addClass("disabled").attr('disabled',true);
	$('button.multiselect').addClass('disabled');
}